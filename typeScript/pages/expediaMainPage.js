"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
class expediaMainPage {
    constructor() {
        this.fromAirport = protractor_1.$("input[id='flight-origin-flp']");
        this.toAirport = protractor_1.$("input[id='flight-destination-flp']");
        this.departingDate = protractor_1.$("input[id='flight-departing-flp']");
        this.returningDate = protractor_1.$("input[id='flight-returning-flp']");
        this.searchButton = protractor_1.$("#gcw-flights-form-flp > div:nth-child(22) > label > button");
    }
}
exports.expediaMainPage = expediaMainPage;
