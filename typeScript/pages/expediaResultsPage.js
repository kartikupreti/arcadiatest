"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
class expediaResultsPage {
    constructor() {
        this.nonStopCheckBox = protractor_1.$("input[id='stopFilter_stops-0']");
        this.lowestNonstopFlight = protractor_1.$("#stops > label:nth-child(2) > span.inline-label.from-price");
        this.firstFilterCheckBox = protractor_1.$("label[class='check filter-option']");
    }
}
exports.expediaResultsPage = expediaResultsPage;
