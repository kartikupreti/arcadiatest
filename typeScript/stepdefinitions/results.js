"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
const expediaResultsPage_1 = require("../pages/expediaResultsPage");
const fs_extra_1 = require("fs-extra");
const config_1 = require("../config/config");
const { Given, When, Then } = require("cucumber");
const chai = require("chai").use(require("chai-as-promised"));
const expect = chai.expect;
const assert = chai.assert;
exports.cost = [];
const rp = new expediaResultsPage_1.expediaResultsPage();
Then(/^I see the results page$/, () => __awaiter(this, void 0, void 0, function* () {
    yield expect(protractor_1.browser.getTitle()).to.eventually.have.string("| Expedia");
}));
When(/^I select the non stop option$/, () => __awaiter(this, void 0, void 0, function* () {
    yield rp.nonStopCheckBox.isDisplayed(); // if fails no nonstop fligts found
    // await expect(rp.nonStopCheckBox.isDisplayed()).to.equal(true, "No non stop flights found"); // if fails no nonstop fligts found
    yield rp.firstFilterCheckBox.click();
}));
Then(/^I get the lowest price for "([^"]*)" to  "([^"]*)"$/, (origin, destination) => __awaiter(this, void 0, void 0, function* () {
    let text1 = yield rp.lowestNonstopFlight.getText();
    yield fs_extra_1.appendFile("./output.txt", text1);
    yield fs_extra_1.appendFile("./output.txt", origin);
}));
Then(/^I get the lowest price "(.*?)"$/, (num) => __awaiter(this, void 0, void 0, function* () {
    let cost1 = yield rp.lowestNonstopFlight.getText();
    let text1 = yield rp.lowestNonstopFlight.getText();
    yield fs_extra_1.appendFile("./output.txt", text1 + '\n');
    //cost[num] = cost1;
}));
Then(/^I get the lowest price$/, () => __awaiter(this, void 0, void 0, function* () {
    let min = 0;
    for (let i = 0; i < 3; i++) {
        if (exports.cost[i] < min) {
            min = exports.cost[i];
        }
    }
    yield fs_extra_1.appendFile("./output.txt", min);
}));
Then(/^I navigate to home page$/, () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser.get(config_1.config.baseUrl);
}));
