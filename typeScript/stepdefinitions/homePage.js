"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
const expediaMainPage_1 = require("../pages/expediaMainPage");
const { Given, When, Then } = require("cucumber");
const chai = require("chai").use(require("chai-as-promised"));
const expect = chai.expect;
const mp = new expediaMainPage_1.expediaMainPage();
Given(/^I am on expedia main page$/, () => __awaiter(this, void 0, void 0, function* () {
    yield expect(protractor_1.browser.getTitle()).to.eventually.equal("Cheap Flights: Find One-Way Airline Tickets & Airfare Deals | Expedia");
}));
When(/^I enter airport information from : "(.*?)" to : "(.*?)" departing on : "(.*?)" and returning on : "(.*?)"$/, (origin, destination, departingDate, returningDate) => __awaiter(this, void 0, void 0, function* () {
    yield mp.fromAirport.sendKeys(origin);
    yield mp.toAirport.sendKeys(destination);
    yield mp.returningDate.clear();
    yield mp.returningDate.sendKeys(returningDate);
    yield mp.departingDate.clear();
    yield mp.departingDate.sendKeys(departingDate);
}));
Then(/^I click search$/, () => __awaiter(this, void 0, void 0, function* () {
    yield protractor_1.browser.actions().sendKeys(protractor_1.protractor.Key.ENTER).perform();
    yield protractor_1.browser.sleep(10000); // cuz expedia page is not angular for some parts, better fix is to ignoresync .
}));
