import {browser} from "protractor";
import {expediaResultsPage} from "../pages/expediaResultsPage";
import {appendFile} from "fs-extra";
import {config} from "../config/config";


const {Given, When, Then} = require("cucumber");

const chai = require("chai").use(require("chai-as-promised"));
const expect = chai.expect;
const assert = chai.assert;
export let cost: number[] = [];

const rp: expediaResultsPage = new expediaResultsPage();

Then(/^I see the results page$/, async () => {
    await expect(browser.getTitle()).to.eventually.have.string("| Expedia");
});

When(/^I select the non stop option$/, async () => {
    await rp.nonStopCheckBox.isDisplayed(); // if fails no nonstop fligts found
   // await expect(rp.nonStopCheckBox.isDisplayed()).to.equal(true, "No non stop flights found"); // if fails no nonstop fligts found
    await rp.firstFilterCheckBox.click();
});

Then(/^I get the lowest price for "([^"]*)" to  "([^"]*)"$/, async (origin, destination) => {
    let text1 = await rp.lowestNonstopFlight.getText();
    await appendFile("./output.txt", text1);
    await appendFile("./output.txt", origin);
});

Then(/^I get the lowest price "(.*?)"$/, async (num) => {
    let cost1: number = await rp.lowestNonstopFlight.getText();
    let text1 = await rp.lowestNonstopFlight.getText();

    await appendFile("./output.txt", text1 + '\n');
    //cost[num] = cost1;

});

Then(/^I get the lowest price$/, async () => {
    let min = 0;
    for (let i = 0; i < 3; i++) {
        if (cost[i] < min) {
            min = cost[i];
        }
    }
    await appendFile("./output.txt", min);

});

Then(/^I navigate to home page$/, async () => {
    await browser.get(config.baseUrl);
});
