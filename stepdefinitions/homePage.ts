import {browser, protractor} from "protractor";
import {expediaMainPage} from "../pages/expediaMainPage";

const {Given, When, Then} = require("cucumber");
const chai = require("chai").use(require("chai-as-promised"));
const expect = chai.expect;

const mp: expediaMainPage = new expediaMainPage();

Given(/^I am on expedia main page$/, async () => {
    await expect(browser.getTitle()).to.eventually.equal("Cheap Flights: Find One-Way Airline Tickets & Airfare Deals | Expedia");
});

When(/^I enter airport information from : "(.*?)" to : "(.*?)" departing on : "(.*?)" and returning on : "(.*?)"$/, async (origin, destination, departingDate, returningDate) => {
    await mp.fromAirport.sendKeys(origin);
    await mp.toAirport.sendKeys(destination);
    await mp.returningDate.clear();
    await mp.returningDate.sendKeys(returningDate);
    await mp.departingDate.clear();
    await mp.departingDate.sendKeys(departingDate);

});


Then(/^I click search$/, async () => {

    await browser.actions().sendKeys(protractor.Key.ENTER).perform();
    await browser.sleep(10000); // cuz expedia page is not angular for some parts, better fix is to ignoresync .
});



