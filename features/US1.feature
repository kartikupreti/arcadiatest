Feature: To test the to-do app based on the given user story 1

  @CucumberScenario
  Scenario Outline: Cucumber search flights on expedia
    Given I am on expedia main page
    When I enter airport information from : "<from>" to : "<to>" departing on : "<departing>" and returning on : "<returning>"
    When I click search
    Then I see the results page
    When I select the non stop option
    Then I get the lowest price "<window>"
    Then I navigate to home page


    Examples:
      | from | to  | departing  | returning  | window |
      | SEA  | SJC | 04/05/2018 | 04/12/2018 | 0     |
      | SEA  | SJC | 04/06/2018 | 04/13/2018 | 1     |
      | SEA  | SJC | 04/07/2018 | 04/14/2018 | 2     |