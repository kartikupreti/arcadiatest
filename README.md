# README #

###Steps to get up and running ###
*checkout https://kartikupreti@bitbucket.org/kartikupreti/arcadiatest.git

	*npm run webdriver-update
	*npm run webdriver-start
	let it run either in back ground with & or in a separate terminal 
	*npm run build 
	This will compile the js files
	
	
### What is this repository for? ###

** Reliable e2e UI testing using protractor and cucumber
** Version 1.0


### How do I get set up? ###
Configure
	1. config.ts
	2. tsconfig.json

### How to run tests###
	npm test
To add more just create more feature files and when they are ready to be run put the @CucumberScenario tag above the scenarios.
>For further tweaking modify the "test" script in package.json


### How do I view the results ###

You will find a summary of the results at the /reports folder. 
Currently we have html and Json reports. 


### Other run scripts ###
	 "build": "tsc",
    "clean": "rimraf typeScript/",
    "clean-build": "npm run clean && npm run build",
    "test": "protractor typeScript/config/config.js",
    "webdriver-update": "webdriver-manager update",
    "webdriver-start": "webdriver-manager start"
	
	>use as npm run <script>






Kartik Upreti