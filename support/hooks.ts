const {BeforeAll, After, Status, AfterAll, setDefaultTimeout} = require("cucumber");
import {browser} from "protractor";
import {config} from "../config/config";
import {cost} from "../stepdefinitions/results"
import {appendFile} from "fs-extra";

BeforeAll({timeout: 30 * 1000}, async () => {
    await setDefaultTimeout(30000);
    await browser.get(config.baseUrl,30000);
});

After(async function (scenario) {
    if (scenario.result.status === Status.FAILED) {
        // screenShot is a base-64 encoded PNG
        const screenShot = await browser.takeScreenshot();
        this.attach(screenShot, "image/png");
    }
});

