import { $ } from "protractor";

export class expediaResultsPage {
    public nonStopCheckBox: any;
    public lowestNonstopFlight: any;
    public firstFilterCheckBox: any;

    constructor() {
        this.nonStopCheckBox = $("input[id='stopFilter_stops-0']");
        this.lowestNonstopFlight = $("#stops > label:nth-child(2) > span.inline-label.from-price");
        this.firstFilterCheckBox = $("label[class='check filter-option']");
    }
}
