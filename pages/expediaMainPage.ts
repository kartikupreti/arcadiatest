import {$} from "protractor";

export class expediaMainPage {
    public fromAirport: any;
    public toAirport: any;
    public departingDate: any;
    public returningDate: any;
    public searchButton: any;

    constructor() {
        this.fromAirport = $("input[id='flight-origin-flp']");
        this.toAirport = $("input[id='flight-destination-flp']");
        this.departingDate = $("input[id='flight-departing-flp']");
        this.returningDate = $("input[id='flight-returning-flp']");
        this.searchButton = $("#gcw-flights-form-flp > div:nth-child(22) > label > button");
    }
}
